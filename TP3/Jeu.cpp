#include "Jeu.h"


Jeu::Jeu(GrilleMorpion grille)
{
    _nbTour=1;
    bool finJeu=false;

    grille.Affichage();
    while(!finJeu){ //boucle pour fin du jeu

        //Définition du joueur actif
        grille.joueurActif=grille.GetJoueurs()[_nbTour%2];
        int x,y=0;
        bool tourSuivant=false;

        while(!tourSuivant){ //boucle pour passer au tour suivant
            std::cout << std::endl<<"Joueur "<< grille.joueurActif.GetValeur()<<std::endl;
            std::cout << "Entrez les coordonnees de la case ou vous voulez placer un jeton"<< std::endl;
            std::cin >>x>>y;
            
            if(!VerifSaisie(x,grille.GetNombreLignes())||!VerifSaisie(y,grille.GetNombreLignes())){
                std::cout<< "Entrez une valeur correcte (entre 1 et "<< grille.GetNombreLignes()<< ")"<<std::endl<<std::endl;
            }
            else{
                x--;
                y--;
                //Vérification si la case est vide
                if(!grille.CaseVide(x,y)){
                    std::cout<<"Case deja occupee, choisissez une autre case" <<std::endl<<std::endl<<std::endl;
                }
                else{
                    tourSuivant=true;
                    std::cout <<std::endl<<std::endl;
                }
            }
        }

        grille.DeposerJeton(x,y);
        grille.Affichage();

        //Vérification si victoire
        if(grille.VictoireJoueur(grille.joueurActif)){
            std::cout << "Victoire du Joueur "<<grille.joueurActif.GetValeur()<<std::endl;
            finJeu =true;
        }
        else if (grille.EstPleine()){
            std::cout <<"La grille est pleine"<<std::endl;
            finJeu=true;
        }
        else{
            _nbTour++;
            std::cout <<"\n==================================================================="<<std::endl<<std::endl;
        }
    }
}

Jeu::Jeu(GrillePuissance4 grille){
    _nbTour=1;
    bool finJeu=false;

    grille.Affichage();
    while(!finJeu){ //boucle pour fin du jeu

        //Définition du joueur actif
        grille.joueurActif=grille.GetJoueurs()[_nbTour%2];
        int y=0;
        bool tourSuivant=false;

        while(!tourSuivant){ //boucle pour passer au tour suivant
            std::cout << std::endl<<"Joueur "<< grille.joueurActif.GetValeur()<<std::endl;
            std::cout << "Entrez le numero de la colonne ou vous voulez placer un jeton"<< std::endl;
            std::cin >>y;
            
            if(!VerifSaisie(y,7)){
                std::cout<< "Entrez une valeur correcte (entre 1 et 7)"<<std::endl<<std::endl;
            }
            else{
                y--;
                //Vérification si la case est vide
                if(!grille.ColonnePleine(y)){
                    std::cout<<"Colonne pleine, choisissez une autre colonne" <<std::endl<<std::endl<<std::endl;
                }
                else{
                    tourSuivant=true;
                    std::cout <<std::endl<<std::endl;
                }
            }
        }

        grille.DeposerJeton(y);
        grille.Affichage();

        //Vérification si victoire
        if(grille.VictoireJoueur(grille.joueurActif)){
            std::cout << "Victoire du Joueur "<<grille.joueurActif.GetValeur()<<std::endl;
            finJeu =true;
        }
        else if (grille.EstPleine()){
            std::cout <<"La grille est pleine"<<std::endl;
            finJeu=true;
        }
        else{
            _nbTour++;
            std::cout <<"\n==================================================================="<<std::endl<<std::endl;
        }
    }
}
bool Jeu :: VerifSaisie(int x, int max)const{
    if (x>=1&&x<=max){
        return true;
    }
    else{
        return false;
    }
}

