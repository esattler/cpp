#include "Jeu.h"
#include "GrilleMorpion.h"
#include "GrillePuissance4.h"

int main()
{
    int val;
    bool aChoisi = false;
    while (!aChoisi)
    {
        std::cout << std::endl
                  << std::endl;
        std::cout << "Choisissez un jeu :" << std::endl;
        std::cout << "   1- Morpion" << std::endl;
        std::cout << "   2- Puissance 4" << std::endl;
        std::cin >> val;

        if (val ==1 || val== 2)
        {
            aChoisi=true;
        }
        else
        {
            std::cout << "Choisissez une valeur correcte (1 ou 2)" << std::endl;
        }
    }
    if (val == 1)
    {
        Jeu j{GrilleMorpion()};
    }
    else
    {
        Jeu j{GrillePuissance4()};
    }
}

/**
 * Question bonus
 * Il faudrait créer une classe Grille de laquelle hériterait les classes GrilleMorpion, GrillePuissance4
 * et où on définirait un attribut grille et des méthodes virtuelles
 */