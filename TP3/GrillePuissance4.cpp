#include "GrillePuissance4.h"

//Constructeur
GrillePuissance4::GrillePuissance4()
{
    Joueur j1('R');
    Joueur j2('J');
    _joueurs = {j1, j2};
    for (int i = 0; i < _grille.size(); i++)
    {
        for (int j = 0; j < _grille[0].size(); j++)
        {
            _grille[i][j] = 0;
        }
    }
    //std::cout<<_grille[0].size(); 7
    //std::cout<<_grille.size(); 4
}

/**
 * Renvoi si la colonne est pleine
 * true: elle est pleine
 * false : elle ne l'est pas
 */
bool GrillePuissance4 ::ColonnePleine(int c)const
{
    if (_grille[0][c] != 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * Place un jeton dans la colonne passée en paramètre
 */
void GrillePuissance4 ::DeposerJeton(int c)
{
    for(int i=3; i>=0; i--)
    {
        if( _grille[i][c] == 0)
        {
            _grille[i][c] = joueurActif.GetValeur();
            break;
        }
    }    
    //std::cout << "[" << i << "," << c << "]=" << _grille[i][c]<<std::endl;
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la ligne comporte 4 jetons alignés
 * false: si elle n'en possède pas
 */
bool GrillePuissance4 ::AligneColonne(int c, Joueur joueur)const
{
    int compt=0;
    for (int i = 0; i < _grille.size(); i++)
    {
        if (_grille[i][c] == joueur.GetValeur())
        {
            compt++;
        }
        else{
            compt=0;
        }
    }
    if(compt>=4){
        return true;
    }
    else{
        return false;
    }
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la colonne comporte 4 jetons alignés
 * false: si elle n'en possède pas
 */
bool GrillePuissance4 ::AligneLigne(int l, Joueur joueur)const
{
    int compt=0;
    for (int i = 0; i < _grille[0].size(); i++)
    {
        if (_grille[l][i] == joueur.GetValeur())
        {
            compt++;
            if (compt >=4){
                return true;
            }
        }
        else{
            compt=0;
        }
    }
    return false;
}
/**
 * Renvoi pour le joueur en paramètre
 * true : si la diagonale comporte 4 jetons alignés
 * false: si elle n'en possède pas
 */
bool GrillePuissance4 ::AligneDiagonale(Joueur joueur)const
{
    for (int x = 0; x < 4; x++) {
        int i = 3;
            int compt = 0;
            for (int j = x; j < x+4; j++) {
                if(_grille[i][j] == joueur.GetValeur()){
                    compt++;
                    if (compt == 4){
                        return true;
                    }
                } else {
                    compt = 0;
                }
                i--;
            }
        }
    
    for (int x = 0; x < 4; x++) {
        int k = 0;
            int compt = 0;
            for (int j = x; j < x+4; j++) {
                if(_grille[k][j] == joueur.GetValeur()){
                    compt++;
                    if (compt == 4){
                        return true;
                    }
                } else {
                    compt = 0;
                }
                k++;
            }
        }
    
    return false;

}

/**
 * Renvoi:
 * true : si le joueur a gagné
 * false : si le joueur a perdu
 */
bool GrillePuissance4 ::VictoireJoueur(Joueur joueur)const
{
    bool victoire=false;
    for (int i = 0; i < _grille.size(); i++)
    {
        if (AligneLigne(i, joueur)){
            victoire = true;
        }
    }
    for (int i = 0; i < _grille[0].size(); i++)
    {
        if (AligneColonne(i, joueur) )
        {
            victoire = true;
        }
    }
    if (!victoire && (AligneDiagonale( joueur)))
    {
        victoire = true;
    }
    return victoire;
}

/**
 * Affiche la grille
 */
void GrillePuissance4 ::Affichage()const
{
    for (int i = 0; i < _grille.size(); i++)
    {
        for (int j = 0; j < _grille[0].size(); j++)
        {

            if (j > 0)
            {
                std::cout << " | ";
            }
            //std::cout << "[" << i << "," << j << "]=" << _grille[i][j];
            std::cout << _grille[i][j];
            if (j == _grille[0].size()-1)
            {
                std::cout << std::endl;
            }
        }
    }
}

/**
 * Retourne la liste des joueurs
 */
std::array<Joueur, 2> GrillePuissance4 ::GetJoueurs()const
{
    return _joueurs;
}

/**
 * Retourne
 * true: si la grille est pleine
 * false: si ce n'est pas le cas
 */
bool GrillePuissance4::EstPleine()const
{
    bool estPleine = true;
    for (int i = 0; i < _grille.size(); i++)
    {
        for (int j = 0; j < _grille[0].size(); j++)
        {
            if (_grille[i][j] == 0)
            {
                estPleine = false;
            }
        }
    }
    return estPleine;
}
