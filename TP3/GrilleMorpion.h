#ifndef MORPION_H
#define MORPION_H
#include <iostream>
#include <array>
#include "Joueur.h"

class GrilleMorpion{
public :
    GrilleMorpion();
    bool CaseVide (int l, int c)const ;
    void DeposerJeton(int l, int c);
    bool LigneComplete(int ligne, Joueur joueur) const;
    bool ColonneComplete(int colonne, Joueur joueur) const;
    bool DiagonaleComplete(int diagonale, Joueur joueur)const;
    bool VictoireJoueur(Joueur joueur)const;
    void Affichage() const;
    std::array<Joueur,2> GetJoueurs()const;
    unsigned long int GetNombreLignes() const;
    bool EstPleine()const;

    Joueur joueurActif;

private:
    std::array<std::array<char, 3>,3> _grille;
    std::array<Joueur,2>_joueurs;
};

#endif // MORPION_H