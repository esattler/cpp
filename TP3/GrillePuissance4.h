#ifndef PUISSANCE_H
#define PUISSANCE_H
#include <iostream>
#include <array>
#include "Joueur.h"

class GrillePuissance4
{
public:
    GrillePuissance4();
    bool ColonnePleine(int c) const ;
    void DeposerJeton(int c);
    bool AligneLigne(int l, Joueur joueur) const;
    bool AligneColonne(int c, Joueur joueur) const;
    bool AligneDiagonale( Joueur joueur)const;
    bool VictoireJoueur(Joueur joueur)const;
    void Affichage()const;
    std::array<Joueur, 2> GetJoueurs()const;
    Joueur joueurActif;
    bool EstPleine()const;

private:
    std::array<std::array<char, 7>, 4> _grille;
    std::array<Joueur, 2> _joueurs;
};
/**
 * Après réflexion j'aurai mis joueurActif en privé et mis des get/set. de meme pour GrilleMorpion
 */

#endif // PUISSANCE_H