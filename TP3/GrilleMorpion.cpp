#include "GrilleMorpion.h"

//Constructeur
GrilleMorpion::GrilleMorpion()
{
    Joueur j1('X');
    Joueur j2('O');
    _joueurs = {j1, j2};
    for (int i = 0; i < _grille.size(); i++)
    {
        for (int j = 0; j < _grille.size(); j++)
        {
            _grille[i][j] = 0;
        }
    }
}

/**
 * Renvoi si une case passée en paramètre est vide
 * true: elle est vide
 * false : elle est occupée
 */
bool GrilleMorpion ::CaseVide (int l, int c) const
{
    if (_grille[l][c] == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Place un jeton dans la case passée en paramètre
 */
void GrilleMorpion ::DeposerJeton(int l, int c)
{
    _grille[l][c] = joueurActif.GetValeur();
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la ligne passée en paramètre est pleine
 * false: si elle ne l'est pas
 */
bool GrilleMorpion ::LigneComplete (int l, Joueur joueur) const
{
    bool res = true;
    for (int i = 0; i < _grille.size(); i++)
    {

        if (_grille[l][i] != joueur.GetValeur())
        {
            res = false;
        }
    }
    return res;
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la colonne passée en paramètre est pleine
 * false: si elle ne l'est pas
 */
bool GrilleMorpion ::ColonneComplete (int c, Joueur joueur) const
{
    bool res = true;
    for (int i = 0; i < _grille.size(); i++)
    {

        if (_grille[i][c] != joueur.GetValeur())
        {
            res = false;
        }
    }
    return res;
}
/**
 * Renvoi pour le joueur en paramètre
 * true : si la diagonale passée en paramètre est pleine
 * false: si elle ne l'est pas
 *
 * diagonale :
 * 0 celle débutant en haut à gauche
 * 1 celle débutant en haut à droite
 */
bool GrilleMorpion ::DiagonaleComplete(int diagonale, Joueur joueur) const
{
    bool res = true;

    if (diagonale == 0)
    {
        for (int i = 0; i< _grille.size(); i++)
        {
            //Vérifie que la case en i, j corresponde au bon joueur
            if (_grille[i][i] != joueur.GetValeur())
            {
                res = false;
            }
        }
    }
    else
    {
        int j = 2;
        for (int i = 0; i <_grille.size(); i++)
        {
            if (_grille[i][j] != joueur.GetValeur())
            {
                res = false;
            }
            j--;
        }
    }
    return res;
}

/**
 * Renvoi:
 * true : si le joueur a gagné
 * false : si le joueur a perdu
 */
bool GrilleMorpion ::VictoireJoueur(Joueur joueur) const
{
    bool victoire;
    for (int i = 0; i < _grille.size(); i++)
    {
        if (LigneComplete(i, joueur) || ColonneComplete(i, joueur))
        {
            victoire = true;
        }
    }
    if (!victoire && (DiagonaleComplete(0, joueur) || DiagonaleComplete(1, joueur)))
    {
        victoire = true;
    }
    return victoire;
}

/**
 * Affiche la grille
 */
void GrilleMorpion ::Affichage() const
{
    for (int i = 0; i < _grille.size(); i++)
    {
        for (int j = 0; j < _grille.size(); j++)
        {

            if (j > 0)
            {
                std::cout << " | ";
            }
            //std::cout << "[" << i << "," << j << "]=" << _grille[i][j];
            std::cout << _grille[i][j];
            if (j == 2)
            {
                std::cout << std::endl;
            }
        }
    }
}

/**
 * Retourne la liste des joueurs
 */
std::array<Joueur, 2> GrilleMorpion ::GetJoueurs() const
{
    return _joueurs;
}

/**
 * Retourne le nombre de lignes de la grille
 */
unsigned long int GrilleMorpion ::GetNombreLignes() const
{
    return _grille.size();
}

/**
 * Retourne
 * true: si la grille est pleine
 * false: si ce n'est pas le cas
 */
bool GrilleMorpion::EstPleine()const
{
    bool estPleine = true;
    for (int i = 0; i < _grille.size(); i++)
    {
        for (int j = 0; j < _grille.size(); j++)
        {
            if (_grille[i][j] == 0)
            {
                estPleine = false;
            }
        }
    }
    return estPleine;
}
