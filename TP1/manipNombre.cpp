/* Pour complir utiliser console Visual studio 2019 console for dev
cl .\nom du fichier.cpp

exe : .\nomdufichier.exe*/

#include <iostream>
#include <vector>
#include <string>
#include "TP1.h"

int main()
{
    int a = 3;
    int b = 5;
    int c = 3;
    int *d = &a;
    int *e = &b;

    srand(time(NULL));
    // std::cout<<"nb1"<<a<<std::endl;
    // std::cout<<"nb2"<<b<<std::endl;
    //inverser(d, e);
    // std::cout<<"nb1"<<a<<std::endl;
    // std::cout<<"nb2"<<b<<std::endl;
    //remplacePoint(a, b, c);
    //remplaceRef(a, b, c);
    //genTableau();

    // int pt = calculPoints(5);
    // std::cout << "points :" << pt << std::endl;
    //salut();
    //devinette();
    scoreTennis(1,1);
    return 0;
}

//1. Manipulation de nombres
//1.1.1 somme d'entiers
int somme(int nb1, int nb2)
{
    return nb1 + nb2;
}

//1.1.2 inverse nbeur d'entiers
int inverser(int *nb1, int *nb2)
{
    int temp = *nb1; //tampon
    *nb1 = *nb2;
    *nb2 = temp;

    return 0;
}
//1.1.3 remplacement de nbeur
//pointeur
int remplacePoint(int a, int b, int c)
{
    int sum = somme(a, b);
    int *point = &sum;
    c = *point;

    std::cout << "remplacePoint" << std::endl;
    std::cout << "nb1 : " << a << std::endl;
    std::cout << "nb2 : " << b << std::endl;
    std::cout << "nb3 : " << c << std::endl
              << std::endl;

    return 0;
}

//référence
int remplaceRef(int a, int b, int c)
{
    int &temp = a;
    c = temp + b;

    std::cout << "remplaceRef   " << std::endl;
    std::cout << "nb1 : " << a << std::endl;
    std::cout << "nb2 : " << b << std::endl;
    std::cout << "nb3 : " << c << std::endl
              << std::endl;

    return 0;
}

//1.1.4 Génération de tableau
int genTableau()
{
    std::vector<int> tb(10);
    bool estTrie = false;
    int nb;

    //Generation du tableau
    std::cout << "generation du tableau ";
    for (int i = 0; i < tb.size(); i++)
    {
        nb = rand() % 100;
        tb[i] = nb;
    }

    afficherTableau(tb);
    trierTableau(tb);

    return 0;
}

int trierTableau(std::vector<int> tb)
{

    bool estTrie = false;
    int *nb1;
    int *nb2;
    std::vector<int> temp(2);
    while (!estTrie)
    {
        estTrie = true;
        for (int i = 0; i < tb.size(); i++)
        {
            nb1 = &tb[i];
            nb2 = &tb[i - 1];

            if (i != 0 && tb[i] < tb[i - 1])
            {
                inverser(nb1, nb2);
                estTrie = false;
            }
        }
        afficherTableau(tb);
    }

    std::cout << "trie du tableau ";
    afficherTableau(tb);

    return 0;
}

int afficherTableau(std::vector<int> tb)
{

    for (int i = 0; i < tb.size(); i++)
    {
        std::cout << tb[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}

//2. Jeu de Tennis

int scoreTennis(int echange1, int echange2)
{
    int points1 = calculPoints(echange1);
    int points2 = calculPoints(echange2);

    std::cout <<"Score Tennis :"<<std::endl;

    if (echange1 == echange2)
    {
        std::cout << "Egalite !" << std::endl;
        std::cout << "Joueur 1 : "<<points1 << std::endl;
        std::cout << "Joueur 2 : "<<points2 << std::endl;
    }
    else if (echange1 > echange2 + 2 || echange2 > echange1 + 2)
    {
        std ::cout << "Victoire Joueur ";
        if (echange1 < echange2)
        {
            std ::cout << "1" << std::endl;
        }
        else
        {
            std ::cout << "2" << std::endl;
        }
        std::cout << "Joueur 1 : "<<points1 << std::endl;
        std::cout << "Joueur 2 : "<<points2 << std::endl;
    }
    else if (echange1>2 && echange2 >2)
    {
        if (echange1 == echange2 + 1)
        {
            std::cout << "Joueur 1 : "<<points1 << "A" << std::endl;
            std::cout << "Joueur 2 : "<<points2 << std::endl;
        }
        else if (echange2 == echange1 + 1)
        {
            std::cout << "Joueur 1 : "<<points1 << std::endl;
            std::cout << "Joueur 2 : "<<points2 << "A" << std::endl;
        }
    }
    else{
        std::cout << "Joueur 1 : "<<points1 << std::endl;
        std::cout << "Joueur 2 : "<<points2 << std::endl;
    }
    return 0;
}

int calculPoints(int echanges)
{
    int points;
    switch (echanges)
    {
    case 0:
        points = 0;
        break;
    case 1:
        points = 15;
        break;
    case 2:
        points = 30;
        break;
    default:
        points = 40;
        break;
    }

    return points;
}

//3.Inscription dans la console et récupération de la saisie
//3.1.1.  et 3.1.2 Print du prénom et nom + salut
int salut()
{
    char prenom[30];
    char nom[30];
    std::cout << "Entrez votre nom et prenom : ";
    std::cin >> nom >> prenom;
    std::cout << "Salut, " << prenom << " " << nom << std::endl;

    return 0;
}
//3.2.1 et 3.2.2 Deviner nombre aléatoire
int devinette()
{
    int nb = rand() % 1000;
    int nbJoueur;
    bool estTrouve = false;

    while (!estTrouve)
    {
        std::cout << "Deviner un nombre entre 0 et 1000:";
        std::cin >> nbJoueur;

        if (nbJoueur > nb)
        {
            std::cout << "Plus petit" << std::endl;
        }
        else if (nbJoueur < nb)
        {
            std::cout << "Plus grand" << std::endl;
        }
        else
        {
            estTrouve = true;
            std::cout << "Felicitation ! Vous avez trouvé le nombre " << nb << std::endl;
        }
    }

    return 0;
}