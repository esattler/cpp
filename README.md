

#  TP 5

Afin de sélectionner quel programme respecte le plus les principes SOLID, la loi Demeter et le principe Tell don’t ask nous allons comparer point par point chacun des éléments cités auparavant.

## Principes SOLID

Pour les principes SOLID nous avons fait des choix de conception relativement similaires. Effectivement pour le principe S, nous avons tous deux respecté majoritairement le principe, en créant des classes et des méthodes n’ayant qu’une seule fonctionnalité, et évitant un maximum les couplages forts entre les classes. Mais nous avons chacun des améliorations à envisager sur la séparation en plusieurs méthodes du constructeur de la classe Jeu. Cependant Emma a utilisé une autre méthode pour sa classe Jeu.

  

Ensuite pour le principe O, nous ne l’avons pas respecté. Tout d’abord du concept ‘Ouvert’, nos classes GrilleMorpion, GrillePuissance4, sont spécifiques et offrent peu d'opportunités d’en découler des héritages. Puis pour le concept ‘Fermé’, Clément a mis en place une condition pour savoir quel jeu est joué et Emma a créé un constructeur de Jeu par type de grille.

  

Le principe L, étant lié aux héritages et qu’aucun de nous en a mis en place, il n'est pas respecté. Nous ne pouvons pas nous avancer pour notre choix sur ce principe.

  

Enfin, les principes I et D ne sont respectés pour aucun d’entre nous car nous n’avons pas mis en place d’interface. On ne peut donc pas départager les programmes sur ces principes.

 

## Loi Demeter

Nous passons ensuite au respect de la loi Demeter. Nous l’avons tous deux respecté. En effet, toutes les méthodes sont appelées directement par une classe et non par une méthode servant d’intermédiaire entre une classe et cette méthode.

  

## Tell don't ask

Pour finir nous nous attardons au principe Tell don’t ask. Chacun de nos programmes traite les données d’une classe dans la classe elle-même, elles ne sont pas traitées via une autre classe.

  

Après avoir comparé le respect des différents principes SOLID, la loi Demeter et le principe Tell don’t ask. Nous avons choisi que ce projet, celui de Emma, respecte plus ces différents éléments. En effet, nos programmes sont très similaires mais le principe S est plus respecté dans le programme d’Emma.
