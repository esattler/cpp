#include "GrilleMorpion.h"

//Constructeur
GrilleMorpion::GrilleMorpion() : Grille(3,3)
{
    _joueurs = {Joueur('X'),  Joueur ('O')};
}

/**
 * Renvoi si une case passée en paramètre est vide
 * true: elle est vide
 * false : elle est occupée
 */
bool GrilleMorpion ::CaseVide (int l, int c) const
{
    if (_grille[l][c] == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Place un jeton dans la case passée en paramètre
 */
void GrilleMorpion ::DeposerJeton(int l, int c)
{
    _grille[l][c] = GetJoueurActif();
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la ligne passée en paramètre est pleine
 * false: si elle ne l'est pas
 */
bool GrilleMorpion ::LigneComplete () const
{
    int j=0;
    int nb=0;

    for (int i=0; i<LIGNES;i++ ){
        while(j<COLONNES  && _grille[i][j]==GetJoueurActif()){

            nb++;
            j++;
        }
        if (nb==COLONNES){
            return true;
        }
        nb=0;
    }

    return false;
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la colonne passée en paramètre est pleine
 * false: si elle ne l'est pas
 */
bool GrilleMorpion ::ColonneComplete () const
{
    int i=0;
    int nb=0;

    for (int j=0; j<COLONNES; j++){
        while(i<LIGNES  && _grille[i][j]==GetJoueurActif()){

            nb++;
            i++;
        }
        if (nb==COLONNES){
            return true;
        }
        nb=0;
    }

    return false;
}
/**
 * Renvoi pour le joueur en paramètre
 * true : si la diagonale passée en paramètre est pleine
 * false: si elle ne l'est pas
 */
bool GrilleMorpion ::DiagonaleComplete() const
{
    bool res = true;

        for (int i = 0; i< LIGNES; i++)
        {
            //Vérifie que la case en i, j corresponde au bon joueur
            if (_grille[i][i] != GetJoueurActif())
            {
                res = false;
            }
        }

        if (res){
            return res;
        }

        res=true;
        int j = 2;
        for (int i = 0; i < LIGNES; i++)
        {
            if (_grille[i][j] != GetJoueurActif())
            {
                res = false;
            }
            j--;
        }

    return res;
}

/**
 * Renvoi:
 * true : si le joueur a gagné
 * false : si le joueur a perdu
 */
bool GrilleMorpion :: VictoireJoueur () const
{
    bool victoire=false;
    if (LigneComplete()||ColonneComplete()||DiagonaleComplete())
    {
        victoire = true;
    }
    return victoire;
}

