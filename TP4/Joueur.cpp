#include "Joueur.h"

//Constructeurs
Joueur::Joueur(){
    _valeur=0;
}
Joueur::Joueur(char valeur){
    _valeur=valeur;
}
/**
 * Renvoi la valeur du joueur
 */
char Joueur:: GetValeur()const{
    return _valeur;
}

void Joueur :: SetValeur(char c){
    _valeur=c;
}
