#ifndef PUISSANCE_H
#define PUISSANCE_H
#include <iostream>
#include <array>
#include "Joueur.h"
#include "Grille.h"

class GrillePuissance4 : public Grille
{
public:
    GrillePuissance4();
    bool ColonnePleine(int c) const ;
    void DeposerJeton(int c);
    bool VictoireJoueur()const;
    bool AligneLigne() const;
    bool AligneColonne() const;
    bool AligneDiagonale() const;
};

#endif // PUISSANCE_H