#ifndef MORPION_H
#define MORPION_H
#include <iostream>
#include <array>
#include <vector>
#include "Joueur.h"
#include "Grille.h"

class GrilleMorpion : public Grille
{
public:
    GrilleMorpion();
    bool CaseVide(int l, int c) const;
    void DeposerJeton(int l, int c);

    bool LigneComplete() const;
    bool ColonneComplete() const;
    bool DiagonaleComplete() const;

    bool VictoireJoueur() const;
};

#endif // MORPION_H