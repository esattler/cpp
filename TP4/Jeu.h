#ifndef JEU_H
#define JEU_H
#include "Joueur.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include "GrilleMorpion.h"
#include "GrillePuissance4.h"
#include "GrilleOthello.h"

class Jeu
{
public:
    Jeu(GrilleMorpion grille);
    Jeu(GrillePuissance4 grille);
    Jeu(GrilleOthello grille);
    bool VerifSaisie(int x,int max)const;
    
private:
    int _nbTour;

};


#endif // JEU_H
