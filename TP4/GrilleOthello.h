#ifndef OTHELLO_H
#define OTHELLO_H
#include <iostream>
#include <array>
#include <vector>
#include "Joueur.h"
#include "Grille.h"

class GrilleOthello : public Grille
{
public:
    GrilleOthello();
    bool CaseVide(int l, int c) const;
    void DeposerJeton(int l, int c);
    bool VictoireJoueur() const;

    bool SandwichVertical(int l, int c);//const
    bool SandwichHorizontal(int l, int c);
    bool SandwichDiagonal(int l, int c);

    bool PeutPoser(int l, int c);
    void RetournerVecticale(int c, int l1, int l2);
    void RetournerHorizontale(int l, int c1, int c2);
    void RetournerDiagonale(int l1, int c1, int l2, int c2);
    void RetournerDiagonaleInverse(int l1, int c1, int l2, int c2);
};

#endif // OTHELLO_H