#include "GrillePuissance4.h"

//Constructeur
GrillePuissance4::GrillePuissance4() : Grille(4, 7)
{
    _joueurs = {Joueur('R'), Joueur('J')};
}

/**
 * Renvoi si la colonne est pleine
 * true: elle est pleine
 * false : elle ne l'est pas
 */
bool GrillePuissance4 ::ColonnePleine(int c) const
{
    if (_grille[0][c] != 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * Place un jeton dans la colonne passée en paramètre
 */
void GrillePuissance4 ::DeposerJeton(int c)
{
    for (int i = LIGNES-1; i >-1; i--)
    {
        if (_grille[i][c] == 0)
        {
            _grille[i][c] = GetJoueurActif();
            break;
        }
    }
    
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la ligne comporte 4 jetons alignés
 * false: si elle n'en possède pas
 */
bool GrillePuissance4 ::AligneColonne() const
{
    int compt = 0;
    for (int j=0; j<COLONNES;j++) {
        for (int i = 0; i < LIGNES; i++) {
            if (_grille[i][j] == GetJoueurActif()){
                compt++;
            } else {
                compt = 0;
            }
        }

        if (compt >= 4) {
            return true;
        }
    }
    return false;
}

/**
 * Renvoi pour le joueur en paramètre
 * true : si la colonne comporte 4 jetons alignés
 * false: si elle n'en possède pas
 */
bool  GrillePuissance4 ::AligneLigne() const
{
    int compt = 0;
    for (int j=0; j<LIGNES; j++) {
        for (int i = 0; i < COLONNES; i++) {
            if (_grille[j][i] == GetJoueurActif()){
                compt++;
                if (compt >= 4) {
                    return true;
                }
            } else {
                compt = 0;
            }
        }
    }
    return false;
}
/**
 * Renvoi pour le joueur en paramètre
 * true : si la diagonale comporte 4 jetons alignés
 * false: si elle n'en possède pas
 */
bool GrillePuissance4 ::AligneDiagonale() const
{
    for (int x = 0; x < 4; x++)
    {
        int i = 3;
        int compt = 0;
        for (int j = x; j < x + 4; j++)
        {
            if (_grille[i][j] == GetJoueurActif()/*joueur.GetValeur()*/)
            {
                compt++;
                if (compt == 4)
                {
                    return true;
                }
            }
            else
            {
                compt = 0;
            }
            i--;
        }
    }

    for (int x = 0; x < 4; x++)
    {
        int k = 0;
        int compt = 0;
        for (int j = x; j < x + 4; j++)
        {
            if (_grille[k][j] == GetJoueurActif()/*joueur.GetValeur()*/)
            {
                compt++;
                if (compt == 4)
                {
                    return true;
                }
            }
            else
            {
                compt = 0;
            }
            k++;
        }
    }

    return false;
}

/**
 * Renvoi:
 * true : si le joueur a gagné
 * false : si le joueur a perdu
 */
bool GrillePuissance4 ::VictoireJoueur() const
{
    bool victoire = false;

    if (AligneColonne()||AligneLigne()||AligneDiagonale())
    {
        victoire = true;
    }
    return victoire;
}