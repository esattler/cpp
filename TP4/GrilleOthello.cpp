#include "GrilleOthello.h"

//Constructeur
GrilleOthello::GrilleOthello() : Grille(8)
{
    _joueurs = {Joueur('B'), Joueur('N')};
    _grille[3][3] = _joueurs[0].GetValeur();
    _grille[4][4] = _joueurs[0].GetValeur();
    _grille[4][3] = _joueurs[1].GetValeur();
    _grille[3][4] = _joueurs[1].GetValeur();

/**
 * Renvoi si une case passée en paramètre est vide
 * true: elle est vide
 * false : elle est occupée
 */
bool GrilleOthello ::CaseVide(int l, int c) const
{
    if (_grille[l][c] == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Place un jeton dans la case passée en paramètre
 */
void GrilleOthello ::DeposerJeton(int l, int c)
{

    _grille[l][c] = GetJoueurActif();
}

/**
 * Renvoi:
 * true : si le joueur a gagné
 * false : si le joueur a perdu
 */
bool GrilleOthello ::VictoireJoueur() const
{
    int comptTotal = 0;
    int comptJoueur = 0;
    bool victoire = false;
    for (int i = 0; i < LIGNES; i++)
    {
        for (int j = 0; j < COLONNES; j++)
        {
            if (!CaseVide(i, j))
            {
                comptTotal++;
            }
            if (_grille[i][j] == GetJoueurActif()/*joueur.GetValeur()*/)
            {
                comptJoueur++;
            }
        }
    }

    //Si plus de la moitié des cases est au joueur
    if (EstPleine())
    {
        int moitie = LIGNES * COLONNES / 2;
        if (comptJoueur > moitie)
        {
            victoire = true;
        }
    }
    else if (comptTotal == comptJoueur)
    {
        victoire = true;
    }

    return victoire;
}

/**
 * Vérification d"un placement en verticale d'un jeton du joueur actif
 * =Sandwich vetical
 * Renvoie
 *  true : si c'est le cas
 *  false : si non
 */
bool GrilleOthello ::SandwichVertical(int l, int c)
{
    bool res = false;
    int nb = 0; //nombre de jeton de la couleur adverse
    int i = l + 1;

    //Vérif en bas de la case
    if (c != COLONNES)
    {
        while (i < COLONNES && _grille[i][c] != GetJoueurActif() && !CaseVide(i, c)) //Vérifie qu'il y a un jeton de la couleur adverse
        {
            nb++;
            i++;
        }
        if (_grille[i][c] == GetJoueurActif() && nb > 0) // Si le jeton est de la couleur du joueur actif et qu'il y a des jetons entre
        {
            std::cout << "verti bas : true";
            RetournerVecticale(c, l, i);
            res = true;
        }
    }

    //Vérif en haut de la case
    if (l != 0) //&&!res
    {
        nb = 0;
        i = l - 1;

        while (i > -1 && _grille[i][c] != GetJoueurActif() && !CaseVide(i, c))
        {
            nb++;
            i--;
        }
        if (_grille[i][c] == GetJoueurActif() && nb > 0)
        {
            res = true;
            RetournerVecticale(c, i, l);
            std::cout << "verti haut : true";
        }
    }

    if (!res)
    {
        std::cout << "Pas vertical";
    }
    return res;
}

/**
 * Vérification d"un placement en horizontal d'un jeton du joueur actif
 * =Sandwich horizontal
 * Renvoie
 *  true : si c'est le cas
 *  false : si non
 */
bool GrilleOthello ::SandwichHorizontal(int l, int c)
{
    bool res = false;
    int nb = 0; //nombre de jeton de la couleur adverse
    int i = c + 1;

    //Vérif en à droite de la case
    if (c != COLONNES)
    {
        while (i < COLONNES && _grille[l][i] != GetJoueurActif() && !CaseVide(l, i))
        {
            nb++;
            i++;
        }
        if (_grille[l][i] == GetJoueurActif() && nb > 0)
        {
            std::cout << "hori droite : true";
            RetournerHorizontale(l, c, i);
            res = true;
        }
    }

    //Vérif à gauche de la cases
    if (c != 0) // && !res
    {
        nb = 0;
        i = c - 1;

        while (i > -1 && _grille[l][i] != GetJoueurActif() && !CaseVide(l, i))
        {
            nb++;
            i--;
        }
        if (_grille[l][i] == GetJoueurActif() && nb > 0)
        {
            res = true;
            RetournerHorizontale(l, i, c);
            std::cout << "hori gauche : true";
        }
    }

    if (!res)
    {

        std::cout << "Pas horizontal";
    }
    return res;
}

/**
 * Vérification d"un placement en diagonale d'un jeton du joueur actif
 * =Sandwich diagonal
 * Renvoie
 *  true : si c'est le cas
 *  false : si non
 */
bool GrilleOthello ::SandwichDiagonal(int l, int c)
{
    Affichage();
    bool res = false;
    int nb = 0; //nombre de jeton de la couleur adverse
    int i = l - 1;
    int j = c - 1;

    //Vérification haut gauche
    if (l != 0 && c != 0)
    {
        while (i > -1 && j > -1 && _grille[i][j] != GetJoueurActif() && !CaseVide(i, j))
        {
            std::cout << "hg[" << i << ", " << j << "]" << std::endl;
            nb++;
            i--;
            j--;
        }
        if (_grille[i][j] == GetJoueurActif() && nb > 0)
        {
            res = true;
            RetournerDiagonale(i, j, l, c);
            std::cout << "diag haut gauche : true" << std::endl;
        }
    }

    //Vérification haut droit
    if (l != 0 && c != COLONNES)
    {
        i = l - 1;
        j = c + 1;

        while (i > -1 && j < COLONNES-1 && _grille[i][j] != GetJoueurActif() && !CaseVide(i, j))
        {
            std::cout << "hd[" << i << ", " << j << "]" << std::endl;
            nb++;
            i--;
            j++;
        }
        if (_grille[i][j] == GetJoueurActif() && nb > 0)
        {
            res = true;
            RetournerDiagonaleInverse(i, j, l, c);
            std::cout << "diag haut droit : true";
        }
    }

    //Vérification bas droite
    if (l != LIGNES && c != COLONNES)
    {
        i = l + 1;
        j = c + 1;
        while (i < LIGNES-1 && j < COLONNES-1 && _grille[i][j] != GetJoueurActif() && !CaseVide(i, j))
        {
            std::cout << "bd[" << i << ", " << j << "]" << std::endl;
            nb++;
            i++;
            j++;
        }
        if (_grille[i][j] == GetJoueurActif() && nb > 0)
        {
            res = true;
            RetournerDiagonale(l, c, i, j);
            std::cout << "diag bas droite : true" << std::endl;
        }
    }

    //Vérification bas gauche
    if (l != LIGNES && c != 0)
    {
        i = l + 1;
        j = c - 1;

        while (i < LIGNES-1 && j > -1 && _grille[i][j] != GetJoueurActif() && !CaseVide(i, j))
        {
            std::cout << "bg[" << i << ", " << j << "]" << std::endl;
            nb++;
            i++;
            j--;
        }
        if (_grille[i][j] == GetJoueurActif() && nb > 0)
        {
            res = true;
            RetournerDiagonaleInverse(l, c, i, j);
            std::cout << "diag bas gauche : true" << std::endl;
        }
    }

    if (!res)
    {
        std::cout << "Pas diagonal";
    }
    return res;
}

void GrilleOthello ::RetournerVecticale(int c, int l1, int l2)
{
    for (int i = l1; i < l2; i++)
    {
        _grille[i][c] = GetJoueurActif();
    }
}
void GrilleOthello ::RetournerHorizontale(int l, int c1, int c2)
{
    for (int i = c1; i < c2; i++)
    {
        _grille[l][i] = GetJoueurActif();
    }
}

/**
 * @brief Retourne les jetons en diagonale
 * 
 * @param l1 
 * @param c1 
 * @param l2 
 * @param c2 
 * @param diagNum numéro de la diagonale
 */
void GrilleOthello ::RetournerDiagonale(int l1, int c1, int l2, int c2) {
    /*int j;
    switch (diagNum)
    {
    case 0: //bas droite
        j = c1;
        for (int i = l1; i < l2 && j < c2; i++)
        {

            _grille[i][j] = GetJoueurActif();
            j++;
        }
        break;

    case 1: //bas gauche
        j = c1;
        for (int i = l1; i < l2 && j < c2; i++)
        {

            _grille[i][j] = GetJoueurActif();
            j--;
        }
        break;
    case 2: //haut gauche
        for (int i = l1; i < l2 && j < c2; i--)
        {

            _grille[i][j] = GetJoueurActif();
            j--;
        }
        break;
    case 3: //huat droit
        for (int i = l1; i < l2 && j < c2; i--)
        {

            _grille[i][j] = GetJoueurActif();
            j++;
        }
        break;
    }

    std::cout << "l1: " << l1 << std::endl;
    std::cout << "l2: " << l2 << std::endl;
    std::cout << "c1: " << c1 << std::endl;
    std::cout << "c2: " << c2 << std::endl;*/
    int j = c1;
    for (int i = l1; i < l2 && j < c2; i++) {
        _grille[i][j] = GetJoueurActif();
        j++;
    }
}
/*
 * Retourne la diagonale opposée
 */
    void GrilleOthello ::RetournerDiagonaleInverse (int l1, int c1, int l2, int c2) {
        int j = c1;
        for (int i = l1; i < l2 && j > c2; i++) {
            _grille[i][j] = GetJoueurActif();
            j--;
        }
}
