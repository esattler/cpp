#include <limits>
#include "Jeu.h"

int main()
{
    int val;
    bool aChoisi = false;
    while (!aChoisi)
    {
        std::cout << std::endl
                  << std::endl;
        std::cout << "Choisissez un jeu :" << std::endl;
        std::cout << "   1- Morpion" << std::endl;
        std::cout << "   2- Puissance 4" << std::endl;
        std::cout << "   3- Othello" << std::endl;
        std::cin >> val;
        std::cout<<std::endl;

        if ((val<1 && val >3)|| std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Entrez une valeur correcte (entre 1 et 3)" << std::endl
                      << std::endl;
        }
        else{
            aChoisi=true;
        }
    }
    if (val == 1)
    {
        Jeu j{GrilleMorpion()};
    }
    else if (val ==2)
    {
        Jeu j{GrillePuissance4()};
    }
    else{
        Jeu j{GrilleOthello()};
    }
    return 0;
}

/**
 * Question bonus
 * Il faudrait créer une classe Grille de laquelle hériterait les classes GrilleMorpion, GrillePuissance4
 * et où on définirait un attribut grille et des méthodes virtuelles
 */