#ifndef GRILLE
#define GRILLE
#include <iostream>
#include <array>
#include <vector>
#include "Joueur.h"

class Grille
{
public:
    //Grille();
    Grille(int ligne, int colonne);
    Grille (int cote);
    //virtual void DeposerJeton(int c);
    virtual bool VictoireJoueur() const{};
    
    bool EstPleine() const;
    char GetJoueurActif() const;
    void Affichage() const;
    void SetJoueurActif(char c);
    void ChangeJoueurActif(int nbTours);
    std::array<Joueur, 2> GetJoueurs() const;
    int GetLignes() const;
    int GetColonnes() const;

protected:
    std::vector<std::vector<char>> _grille;
    std::array<Joueur, 2> _joueurs;
    int LIGNES;
    int COLONNES;

private:
    Joueur _joueurActif;
    
};

#endif // GRILLE