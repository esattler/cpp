#ifndef JOUEUR_H
#define JOUEUR_H


class Joueur
{
private:
    char _valeur;
public:
    Joueur();
    Joueur(char valeur);
    char GetValeur ()const;
    void SetValeur (char c);

};

#endif // JOUEUR_H
