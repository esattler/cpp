#include "Grille.h"

//Constructeur
Grille::Grille(int ligne, int colonne)
{
    LIGNES = ligne;
    COLONNES = colonne;
    _grille.resize(LIGNES, std::vector<char>(COLONNES, 0));
}
Grille::Grille(int cote)
{
    LIGNES = cote;
    COLONNES = cote;
    _grille.resize(LIGNES, std::vector<char>(COLONNES, 0));
}

/**
 * Affiche la grille
 */
void Grille ::Affichage() const
{
    for (int i = 0; i < LIGNES; i++)
    {
        for (int j = 0; j < COLONNES; j++)
        {
            //Afficahge de la valeur
            if (_grille[i][j] == 0)
            {
                std::cout << " ";
            }
            else
            {
                std::cout << _grille[i][j];
            }

            //Affichage des colonnes
            if (j == COLONNES - 1)
            {
                std::cout << std::endl;
            }
            else
            {
                std::cout << " | ";
            }
        }
    }
}

/**
 * Retourne la liste des joueurs
 */
std::array<Joueur, 2> Grille ::GetJoueurs() const
{
    return _joueurs;
}

/**
 * Retourne le nombre de lignes
 */
int Grille ::GetLignes() const
{
    return LIGNES;
}

/**
 * Retourne le nombre de colonnes
 */
int Grille ::GetColonnes() const
{
    return COLONNES;
}

char Grille ::GetJoueurActif() const
{
    return _joueurActif.GetValeur();
}

void Grille ::SetJoueurActif(char c)
{
    _joueurActif.SetValeur(c);
}

void Grille::ChangeJoueurActif(int nbTour)
{
    _joueurActif = _joueurs[nbTour % 2];
}
/**
 * Retourne
 * true: si la grille est pleine
 * false: si ce n'est pas le cas
 */
bool Grille::EstPleine() const
{
    bool estPleine = true;
    for (int i = 0; i < LIGNES; i++)
    {
        for (int j = 0; j < COLONNES; j++)
        {
            if (_grille[i][j] == 0)
            {
                estPleine = false;
            }
        }
    }
    return estPleine;
}
