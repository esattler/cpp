#include <iostream>
#include <vector>
#include <math.h>
#include "Point.h"


/**
 * Classe Triangle
 * _sommet1 ::Point : 1er sommet
 * _sommet2 ::Point : 2nd sommet
 * _sommet3 ::Point : 3e sommet
 */
class Triangle{
    Point _sommet1;
    Point _sommet2;
    Point _sommet3;

    public:
    Triangle(Point sommet1, Point sommet2, Point sommet3);
    void SetSommet1(Point sommet);
    void SetSommet2(Point sommet);
    void SetSommet3(Point sommet);
    Point GetSommet1() const;
    Point GetSommet2() const;
    Point GetSommet3() const;
    float Base();
    float Perimetre();
    float Hauteur();
    float Surface();
    float* Longueurs();
    bool EstIsocele();
    bool EstRectangle();
    bool EstEquilateral();
    void Afficher();
};