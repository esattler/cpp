#ifndef POINT
#define POINT
#include <iostream>

/**
 * Structure de Point
 * x : coordonnée en horizontale
 * y: coordonnée verticale
 */

struct Point
{
    private :
    float _x;
    float _y;

    public :
    Point(float x, float y);
    Point();
    void Afficher();
    float Distance(Point point);  

};
#endif