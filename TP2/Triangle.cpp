#include "Triangle.h"

//Constructeur
Triangle :: Triangle(Point sommet1, Point sommet2, Point sommet3)
{
    this->_sommet1 = sommet1;
    this->_sommet2 = sommet2;
    this->_sommet3 = sommet3;

    Afficher();
}

//Setters
void Triangle ::SetSommet1(Point sommet)
{
    this->_sommet1 = sommet;
}
void Triangle ::SetSommet2(Point sommet)
{
    this->_sommet2 = sommet;
}
void Triangle ::SetSommet3(Point sommet)
{
    this->_sommet3 = sommet;
}

//Getters
Point Triangle ::GetSommet1()const
{
    return this->_sommet1;
}
Point Triangle ::GetSommet2()const
{
    return this->_sommet2;
}
Point Triangle ::GetSommet3()const
{
    return this->_sommet3;
}

/**
 * Renvoi la longueur de la base du triangle
 */
float Triangle ::Base()
{
    float *longueurs = Longueurs();

    if (longueurs[0] > longueurs[1], longueurs[2])
    {
        return longueurs[0];
    }
    else if (longueurs[1] > longueurs[0], longueurs[2])
    {
        return longueurs[1];
    }
    else
    {
        return longueurs[2];
    }
}

/**
 * Renvoi la hauteur du triangle
 */
float Triangle ::Hauteur()
{
    return 2*Surface()/Base();
}

float Triangle ::Perimetre()
{
    float *longueurs = Longueurs();
    return longueurs[0] + longueurs[1] + longueurs[2];
}

/**
 * Renvoi la surface du triangle
 */
float Triangle ::Surface()
{
    float* longueurs = Longueurs();
    float demiPerimetre = (longueurs[0]+longueurs[1]+longueurs[2])/2;

    return sqrt(demiPerimetre*(demiPerimetre-longueurs[0])*(demiPerimetre-longueurs[1])*(demiPerimetre-longueurs[2]));
}

/**
 * Renvoi la longueur des cotés du triangle
 */
float *Triangle ::Longueurs()
{
    float cote1 = this->_sommet1.Distance(this->_sommet2);
    float cote2 = this->_sommet2.Distance(this->_sommet3);
    float cote3 = this->_sommet3.Distance(this->_sommet2);

    float tab[] = {cote1, cote2, cote3};
    return tab;
}

/**
 * Renvoi su le triangle est isolcèle ou non
 * 
 * renvoi true si le triangle est un triangle isocèle
 * renvoi false si ce n'est pas le cas
 */
bool Triangle ::EstIsocele()
{
    float *longueurs = Longueurs();
    bool estIsocele = false;
    int i = 0;
    int j = 0;

    //On compare les longueurs
    while (!estIsocele && i < 3)
    {
        while (!estIsocele && j < 3)
        {
            //on ne compare pas la longueur avec elle meme
            if (j == i)
            {
                j++;
            }
            //On vérifie si 2 cotés ont la meme longueur
            else if (longueurs[i] == longueurs[j])
            {
                estIsocele = true;
            }
            j++;
        }
        i++;
    }
    return estIsocele;
}

/**
 * Renvoi su le triangle est renctangle ou non
 * 
 * renvoi true si le triangle est un triangle rectangle
 * renvoi false si ce n'est pas le cas
 */
bool Triangle ::EstRectangle()
{
    bool estRectangle;
    float *longueurs = Longueurs();
    std::vector<float> triangle(1, 0);

    //On récupère le coté le plus long et on place les valeurs dans un nouveau tableau trié {base, cote1,cote2}
    float base = Base();
    for (int i = 0; i < 3; i++)
    {
        if (longueurs[i] == base)
        {
            triangle[0] = base;
        }
        else
        {
            triangle.push_back(longueurs[i]);
        }
    }

    //On vérifie si le triangle est rectangle
    if (pow(triangle[0], 2) == pow(triangle[1], 2), pow(triangle[2], 2))
    {
        estRectangle = true;
    }
    else
    {
        estRectangle = false;
    }
    return estRectangle;
}

/**
 * Renvoi su le triangle est un triangle equilatéral ou non
 * 
 * renvoi true si le triangle est une triangle équilatéral
 * renvoi false si ce n'est pas le cas
 */
bool Triangle ::EstEquilateral()
{
    bool estEquilateral;
    float *longueurs = Longueurs();
    if (longueurs[0] == longueurs[1] && longueurs[0]== longueurs[2])
    {
        estEquilateral = true;
    }
    else
    {
        estEquilateral = false;
    }
    return estEquilateral;
}

//Affiche le triangle
void Triangle ::Afficher()
{
    std::cout << "sommet 1 : " << std::endl;
    this->_sommet1.Afficher();
    std::cout << "\nsommet 2 : " << std::endl;
    this->_sommet2.Afficher();
    std::cout << "\nsommet 3 : " << std::endl;
    this->_sommet3.Afficher();

    float *lon=Longueurs();
    std::cout << "base :" << Base()<<std::endl;
    std::cout << "\nperimetre :"<<Perimetre()<<std::endl;
    std::cout << "\nhauteur :" << Hauteur()<<std::endl;
    std::cout << "\nsurface :" << Surface()<<std::endl;
    std::cout << "\nlongueurs :" << lon[0] <<" "<<lon[1]<<" "<< lon[2]<<std::endl;
    std::cout << "\nIsocèle :" << EstIsocele()<<std::endl;
    std::cout << "\nRectangle :" << EstRectangle()<<std::endl;
    std::cout << "\nEquilateral :" << EstEquilateral()<<std::endl;
}