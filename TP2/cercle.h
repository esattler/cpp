#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include "Point.h"

/**
 * Structure du Cercle
 * _centre :: Point : centre du cercle
 * _diametre ::int : diamètre du cercle
 */
struct Cercle
{
private:
    Point _centre;
    int _diametre;

public:
    Cercle(int diametre, Point centre);
    int GetDiametre();
    Point GetCentre() const;
    void SetDiametre(int diametre);
    void SetCentre(Point centre);
    float Perimetre();
    float Surface();
    bool EstSurCercle(Point point);
    bool EstDansCercle(Point point);
    void Afficher();
};
