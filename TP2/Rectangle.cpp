#include "Rectangle.h"
//Constructor
Rectangle ::Rectangle(int longueur, int largeur, Point point)
{
    this->_longueur = longueur;
    this->_largeur = largeur;
    this->_point = point;
    Afficher();
}

//Getters
float Rectangle ::GetLongueur()
{
    return this->_longueur;
}
float Rectangle ::GetLargeur()
{
    return this->_largeur;
}
Point Rectangle ::GetPoint() const
{
    return this->_point;
}

//Setters
void Rectangle ::SetLongueur(int longueur)
{
    this->_longueur = longueur;
}
void Rectangle ::SetLargeur(int largeur)
{
    this->_largeur = largeur;
}
void Rectangle ::SetPoint(Point point)
{
    this->_point = point;
}

//Affichage du rectangle
void Rectangle ::Afficher()
{
    std::cout << "longueur : " << this->_longueur << "\nlargeur : " << this->_largeur << std::endl;
    std::cout << "périmètre :" << Perimetre() << std::endl;
    std::cout << "surface :" << Surface() << std::endl;
}

/**
 * Renvoi le primètre du rectangle
 */
float Rectangle ::Perimetre()
{
    return this->_longueur * 2 + 2 * this->_largeur;
}

/**
 * Renvoi la surface du triangle
 */
float Rectangle ::Surface()
{
    return this->_longueur * this->_largeur;
}