#include "Point.h"
#include "Cercle.h"

//Constructeur
Cercle ::Cercle(int diametre, Point centre)
{
    this->_diametre = diametre;
    this->_centre = centre;

    Afficher();
}
//Getters
int Cercle ::GetDiametre()
{
    return this->_diametre;
}
Point Cercle ::GetCentre() const
{
    return this->_centre;
}

//Setters
void Cercle ::SetDiametre(int diametre)
{
    this->_diametre = diametre;
}
void Cercle ::SetCentre(Point centre)
{
    this->_centre = centre;
}

//Renvoi le périmètre du cercle
float Cercle ::Perimetre()
{
    return this->_diametre * M_PI;
}

//Renvoi la surface du cercle
float Cercle ::Surface()
{
    return this->_diametre * M_PI * M_PI;
}

/**
 * Renvoi si le point en paramètre se trouve sur le cercle
 * point :: Point : point où l'on souhiate savoir si il appartient au cercle
 * 
 * renvoi true si le point appartient au cercle
 * renvoi false si il n'y appartient pas
 */
bool Cercle ::EstSurCercle(Point point)
{
    bool surLeCercle;
    float distance = this->_centre.Distance(point);

    if (distance == this->_diametre)
    {
        surLeCercle = true;
    }
    else
    {
        surLeCercle = false;
    }
    return surLeCercle;
}

/**
     * Renvoi si le point en paramètre se trouve dans le cercle
     * point :: Point : point où l'on souhiate savoir si il se trouve dans le cercle
     * 
     * renvoi true si le point se trouve à l'intérieur du cercle
     * renvoi false si il ne s'y trouve pas
     */
bool Cercle ::EstDansCercle(Point point)
{
    bool estDansCercle;
    if (this->_centre.Distance(point) < this->_diametre)
    {
        estDansCercle = true;
    }
    else
    {
        estDansCercle = false;
    }
    return estDansCercle;
}

//Affiche le cercle
void Cercle ::Afficher()
{
    std::cout << "centre : " << std::endl;
    this->_centre.Afficher();
    std::cout << "\ndiamètre : " << this->_diametre << std::endl;
    std::cout << "périmètre :" << Perimetre() << std::endl;
    std::cout << "surface :" << Surface() << std::endl;
}