#include <iostream>
#include "TP2.h"

int main()
{

    std::cout << "Point 1 : " << std::endl;
    Point p(2, 2);
    std::cout << "Point 2 : " << std::endl;
    Point q(0, 2);
    std::cout << "Point 3 : " << std::endl;
    Point u(2, 0);
    std::cout << "\nDistance 1-2: " << p.Distance(q) << std::endl;
    std::cout << "Distance 1-3: " << p.Distance(u) << std::endl;
    std::cout << "Distance 3-2: " << u.Distance(q) << std::endl;

    std::cout << "\nRectangle : " << std::endl;
    Rectangle r(2, 4, p);

    std::cout << "\nTriangle : " << std::endl;
    Triangle t(p, q, u);

    std::cout << "\nCercle : " << std::endl;
    Cercle c(5, p);
    std::cout << "Point 2 sur le cercle ?" << c.EstSurCercle(q) <<std::endl;
    std::cout << "Point 2 est dans le cercle ?" << c.EstDansCercle(q) <<std::endl;

}