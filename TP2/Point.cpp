#include "Point.h"

//Constructeurs
Point :: Point(float x, float y)
{
    this->_x = x;
    this->_y = y;

    Afficher();
}

Point :: Point()
{
    this->_x = 0;
    this->_y = 0;
}

//Affichage de Point
void Point ::Afficher()
{
    std::cout << "x : " << this->_x << "\ny : " << this->_y << std::endl;
}

/**
 * Permet de calculer la distance entre le point actuel et le point passé en paramètre
 * point Point : point avec lequel on souhaite calculer la distance
 */
float Point ::Distance(Point point)
{
    return sqrt(powf(point._x-this->_x,2)+powf(point._y-this->_y,2));
}