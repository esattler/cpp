#include <iostream>
#include "Point.h"

/**
 * Structure Rectangle
 * longueur : longueur du rectangle
 * largeur : largeur du rectangle
 * point : point supérieur gauche
 */
class Rectangle
{
    int _longueur;
    int _largeur;
    Point _point;

    public:
    Rectangle(int longueur, int largeur, Point point);
    float GetLongueur();
    float GetLargeur();
    Point GetPoint() const;
    void SetLongueur(int longueur);
    void SetLargeur(int largeur);
    void SetPoint(Point point);
    void Afficher();
    float Perimetre();
    float Surface();

    
};
